<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<!-- The following line says 'Target the root element of the xml file( XMLTest) to look for data' -->
	<xsl:template match="/">
	<!-- Nice work here Sara.  Your xsl properly formats and outputs the xml data.
You've also added the comment I needed to see as well.  Good work
10/10
-->
		<html>
			<head>
				<title>Customer Info.</title>
			</head>
			<body>
				<!-- Customer personal information selected from elements of 'customer' element -->
				<b>Customer Info</b>
				<br/>
				Name: <xsl:value-of select="telephoneBill/customer/name"/>
				<br/>
				Address: <xsl:value-of select="telephoneBill/customer/address"/>
				<br/>
				City: <xsl:value-of select="telephoneBill/customer/city"/>
				<br/>
				Province: <xsl:value-of select="telephoneBill/customer/province"/>
				<br/>
				<!--Table starts here -->
				<table border="1">
					<tbody>
						<!-- contents of the table are selected from attributes of 'call' element -->
						<tr>
							<th>
								<b>Called Number</b>
							</th>
							<th>
								<b>Date</b>
							</th>
							<th>
								<b>Duration In Minutes</b>
							</th>
							<th>
								<b>Charge</b>
							</th>
						</tr>
						<xsl:for-each select="telephoneBill/call">
							<tr>
								<td>
									<xsl:value-of select="@number"/>
								</td>
								<td>
									<xsl:value-of select="@date"/>
								</td>
								<td>
									<xsl:value-of select="@durationInMinutes"/>
								</td>
								<td>
									<xsl:value-of select="@charge"/>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
